﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using PreschoolFees.Models;

namespace PreschoolFees.ViewModels
{
    public class Calendar
    {
        public List<Week> MonthlyCalendar { get; set; }
        public int ChildId { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public string MonthName { get; set; }
        public List<Meal> Meals { get; set; }
        private PreschoolFeesDbContext dbContext = new PreschoolFeesDbContext();

        public Calendar(int childId, DateTime date)
        {
            ChildId = childId;

            Month = date.Month;
            MonthName = GetMonthName(Month);
            Year = date.Year;

            Meals = dbContext.Meals.ToList();

            DateTime firstDayOfTheMonth = new DateTime(date.Year, date.Month, 1);
            int dayOfTheWeek = (int)firstDayOfTheMonth.DayOfWeek;
            dayOfTheWeek = dayOfTheWeek == 0 ? 7 : dayOfTheWeek;

            DateTime firstDayOfWeek= firstDayOfTheMonth.AddDays(1 - dayOfTheWeek);

            MonthlyCalendar = new List<Week>();
            do
            {
                MonthlyCalendar.Add(new Week(childId, firstDayOfWeek, Meals));
                firstDayOfWeek = firstDayOfWeek.AddDays(7);
            }
            while (firstDayOfWeek.Month == date.Month);
        }

        private string GetMonthName(int month)
        {
            string monthString = "";

            switch (month)
            {
                case 1: monthString = "Styczeń"; break;
                case 2: monthString = "Luty"; break;
                case 3: monthString = "Marzec"; break;
                case 4: monthString = "Kwiecień"; break;
                case 5: monthString = "Maj"; break;
                case 6: monthString = "Czerwiec"; break;
                case 7: monthString = "Lipiec"; break;
                case 8: monthString = "Sierpień"; break;
                case 9: monthString = "Wrzesień"; break;
                case 10: monthString = "Październik"; break;
                case 11: monthString = "Listopad"; break;
                case 12: monthString = "Grudzień"; break;
            }

            return monthString;
        }
    }

    public class Week
    {
        public List<Day> WeekCalendar { get; set; }

        public Week(int childId, DateTime date, List<Meal> meals)
        {
            WeekCalendar = new List<Day>();

            for (int i = 0; i < 7; i++)
            {
                WeekCalendar.Add(new Day(childId, date.AddDays(i), meals));
            }
        }
    }

    public class Day
    {
        private PreschoolFeesDbContext dbContext = new PreschoolFeesDbContext();
        public DateTime Date { get; set; }
        public String DayOfTheWeekShortName { get; set; }
        public Dictionary<Meal, bool> MealEaten { get; set; }
        public bool AllMealsEaten { get; set; }

        public Day(int childId, DateTime date, List<Meal> meals)
        {
            Date = date;
            DayOfTheWeekShortName = getDayOfTheWeekShortName(date);
            AllMealsEaten = true;

            MealEaten = new Dictionary<Meal, bool>();
            List<Meal> mealsNotEaten = dbContext.Attendances
                .Where(x => x.Child.Id == childId)
                .Where(x => DbFunctions.TruncateTime(x.Date) == DbFunctions.TruncateTime(date))
                .Select(x => x.Meal).ToList();

            foreach (Meal meal in meals)
            {
                if (mealsNotEaten.Contains(meal))
                {
                    MealEaten.Add(meal, false);
                    AllMealsEaten = false;
                }
                else
                {
                    MealEaten.Add(meal, true);
                }
                
            }
        }

        private string getDayOfTheWeekShortName(DateTime date)
        {
            String dayOfTheWeekShortName = "";

            switch(date.DayOfWeek)
            {
                case DayOfWeek.Monday: dayOfTheWeekShortName = "Pon"; break;
                case DayOfWeek.Tuesday: dayOfTheWeekShortName = "Wt"; break;
                case DayOfWeek.Wednesday: dayOfTheWeekShortName = "Śr"; break;
                case DayOfWeek.Thursday: dayOfTheWeekShortName = "Czw"; break;
                case DayOfWeek.Friday: dayOfTheWeekShortName = "Pt"; break;
                case DayOfWeek.Saturday: dayOfTheWeekShortName = "Sob"; break;
                case DayOfWeek.Sunday: dayOfTheWeekShortName = "Ndz"; break;
            }

            return dayOfTheWeekShortName;
        }
    }
}