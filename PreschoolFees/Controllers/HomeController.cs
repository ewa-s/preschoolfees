﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PreschoolFees.Models;

namespace PreschoolFees.Controllers
{
    public class HomeController : Controller
    {
        private PreschoolFeesDbContext dbContext = new PreschoolFeesDbContext();

        // GET: Home
        public ViewResult Index()
        {
            return View(dbContext.Children.ToList());
        }


        [HttpPost]
        public ViewResult AttendanceForm(Attendance attendance)
        {
            if (ModelState.IsValid)
            {
                return View("Thanks", attendance);
            }
            else
            {
                return View();
            }
        }
    }
}