﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PreschoolFees.Models;
using PreschoolFees.ViewModels;

namespace PreschoolFees.Controllers
{
    public class CalendarController : Controller
    {
        private PreschoolFeesDbContext dbContext = new PreschoolFeesDbContext();


        // GET: Calendar
        public ActionResult Index(int childId, int? year, int? month)
        {
            DateTime date;

            if (year == null || month == null)
            {
                date = DateTime.Now;
            }
            else
            {
                date = new DateTime((int)year, (int)month, 1);
            }

            Calendar calendar = new Calendar(childId, date);
            return View(calendar);
        }

        public ActionResult ToggleDayAttendance(int childId, DateTime selectedDate)
        {
            List<Attendance> attendances = dbContext.Attendances
                .Where(x => x.Child.Id == childId)
                .Where(x => DbFunctions.TruncateTime(x.Date) == DbFunctions.TruncateTime(selectedDate))
                .ToList();
            
            if (!attendances.Any())
            {
                List<Meal> meals = dbContext.Meals.ToList();
                foreach (var meal in meals)
                {
                    Attendance attendance = new Attendance();
                    attendance.Date = selectedDate;
                    attendance.Present = false;
                    attendance.Child = dbContext.Children.Where(x => x.Id == childId).Single();
                    attendance.Meal = dbContext.Meals.Where(x => x.Id == meal.Id).Single();

                    dbContext.Attendances.Add(attendance);
                    dbContext.SaveChanges();
                }
            }
            else
            {
                dbContext.Attendances.RemoveRange(attendances);
                dbContext.SaveChanges();
            }

            return RedirectToAction("Index", new { childId = childId, year = selectedDate.Year, month = selectedDate.Month });
        }

        public ActionResult ToggleMealAttendance(int childId, DateTime selectedDate, int mealId)
        {
            Attendance attendanceToChange = dbContext.Attendances
                .Where(x => x.Child.Id == childId)
                .Where(x => x.Meal.Id == mealId)
                .Where(x => DbFunctions.TruncateTime(x.Date) == DbFunctions.TruncateTime(selectedDate))
                .SingleOrDefault();

            if (attendanceToChange == null)
            {
                Attendance attendance = new Attendance();
                attendance.Date = selectedDate;
                attendance.Present = false;
                attendance.Child = dbContext.Children.Where(x => x.Id == childId).Single();
                attendance.Meal = dbContext.Meals.Where(x => x.Id == mealId).Single();

                dbContext.Attendances.Add(attendance);
                dbContext.SaveChanges();
            }
            else
            {
                dbContext.Attendances.Remove(attendanceToChange);
                dbContext.SaveChanges();
            }

            return RedirectToAction("Index", new { childId = childId, year = selectedDate.Year, month = selectedDate.Month });
        }

        public ActionResult PreviousMonth(int childId, int currentYear, int currentMonth)
        {
            int month = (currentMonth == 1) ? 12 : currentMonth - 1;
            int year = (currentMonth == 1) ? currentYear - 1 : currentYear;

            return RedirectToAction("Index", new { childId, year, month });
        }

        public ActionResult NextMonth(int childId, int currentYear, int currentMonth)
        {
            int month = (currentMonth == 12) ? 1 : currentMonth + 1;
            int year = (currentMonth == 12) ? currentYear + 1 : currentYear;

            return RedirectToAction("Index", new { childId, year, month });
        }
    }
}