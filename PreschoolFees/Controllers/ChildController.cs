﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PreschoolFees.Models;

namespace PreschoolFees.Controllers
{
    public class ChildController : Controller
    {
        private PreschoolFeesDbContext dbContext = new PreschoolFeesDbContext();

        [HttpGet]
        public ActionResult Index(int childId)
        {
            Child child = dbContext.Children.Find(childId);
            dbContext.Entry(child).Collection(c => c.Attendances).Load();

            if (child == null)
            {
                return HttpNotFound();
            }

            return View(child);
        }
    }
}