﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PreschoolFees.Models
{
    public class Attendance
    {
        [Key]
        public long Id { get; set; }

        public virtual Child Child { get; set; }

        public DateTime Date { get; set; }

        public virtual Meal Meal { get; set; }

        public bool Present { get; set; }
    }
}