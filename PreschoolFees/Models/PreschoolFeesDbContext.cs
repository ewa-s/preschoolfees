﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PreschoolFees.Models
{
    public class PreschoolFeesDbContext : DbContext
    {
        public DbSet<Child> Children { get; set; }
        public DbSet<Attendance> Attendances { get; set; }
        public DbSet<Meal> Meals { get; set; }
    }
}