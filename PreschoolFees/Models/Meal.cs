﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PreschoolFees.Models
{
    public class Meal : IEquatable<Meal>
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<Attendance> Attendances { get; set; }

        public bool Equals(Meal meal)
        {
            if (Id == meal.Id && Name == meal.Name)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}